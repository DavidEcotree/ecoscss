yarn build
aws s3 cp . s3://ecotree-res/site/4.3.0/assets/ --recursive --profile=ecotree --exclude="img/*" --exclude="node_modules/*" --exclude="*.json" --exclude=".git/*" --exclude="scss/*" --acl public-read --cache-control max-age=2592000,public

