
import './scss/ecotree.scss';
import { page_faq } from './modules/page-faq';
import { modal_carbon_footprint } from './modules/modal-carbon-footprint';

window.modal_carbon_footprint = modal_carbon_footprint;


function setup(page) {

  if (page == 'app_site_faq') {
    // page_faq();
  }

  if (page == 'app_site_accueil') {
    // modal_carbon_footprint();
  }

}


window.setup_page_jquery = setup;

export { page_faq, modal_carbon_footprint }