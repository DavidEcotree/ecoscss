$(document).ready(function()
{
    /**
     * Create hover on dropdown menu
     */
    $('body').on('mouseenter mouseleave', '.dropdown-hover', function (e) {
        var _d = $(e.target).closest('.dropdown-hover');
        _d.addClass('show');
        setTimeout(function () {
            _d[_d.is(':hover') ? 'addClass' : 'removeClass']('show');
            $('[data-toggle="dropdown"]', _d).attr('aria-expanded', _d.is(':hover'));
        }, 300);
    });
    
    /**
     * Enable tooltips everywhere
     */
    $('[data-toggle="tooltip"]').tooltip();
    
    /**
     * Enable popover everywhere
     */
    $('[data-toggle="popover"]').popover()
    $('.sharing-box[data-toggle="sharing-popover"]').popover({
        html: true,
        content: function() {
          var content = $(this).attr('data-popover-content');
          return $(content).children('.popover-body').html();
        },
        title: function() {
          var title = $(this).attr('data-popover-content');
          return $(title).children('.popover-heading').html();
        }
    });
    
    /**
     * Enable Slick Slider
     */
    $('#customer-slider').slick({
        autoplay: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        swipeToSlide: true,

        //dots:true
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ]
    });
    
    /**
     * Compactify the header by runining some functions
     */
    $(window).ready(function () // Compactify the header when DOM is ready
    {
        compactifyTheHeader();
    });

    $(window).scroll(function () // Compactify the header when user scrolling
    {
        compactifyTheHeader();
    });
    
    /**
     * Functions whose role is to compactify the header by checking that we are 
     * in a case where the screen is less than the breaking point SM.
     * It also replaces the logo for a slogan-free version.
     */
    function compactifyTheHeader(heightHeaderMax = 150)
    {
        if ($(window).scrollTop() >= heightHeaderMax)
        {
            $('#brand').attr('src', $('#brand').attr('data-compactified'));
            $('#header-menu').removeClass('not-compacted');
            setMarginToTopPage('#header');
        }
        else
        {
            $('#brand').attr('src', $('#brand').attr('data-uncompactified'));
            $('#header-menu').addClass('not-compacted');
            setMarginToTopPage('#header');
        }
    }
    
    /**
     * Get height of fixed header to set as margin-top to the next element
     */
    function setMarginToTopPage(element)
    {
        $(element).each( function() {
            var height = $(this).outerHeight();
            $(this).next().css( {
                'margin-top' : height + 'px' 
            });
        });
    }
	
	
	
	
	
	$('#alert-cookie').removeClass("d-none");
	$('.accept-cookie').click(function () {			
		Cookies.set('cookies_acceptes','oui', { expires: 14 });
		$(this).closest('#alert-cookie').addClass("d-none");
	});	
	if(Cookies.get('cookies_acceptes') !== undefined) {
		$('#alert-cookie').addClass("d-none");			
	}
	

		
	$('#promo_bar').removeClass("d-none");
	$('#promo_bar_close').click(function () {
		console.log('close');
		Cookies.set('promo_bar_closed','oui', { expires: 3 });		
		$('#promo_bar').addClass("d-none");
		compactifyTheHeader();
	});	
	if(Cookies.get('promo_bar_closed') !== undefined) {
		$('#promo_bar').addClass("d-none");			
	}
	

	
	
});