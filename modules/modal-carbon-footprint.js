
export function modal_carbon_footprint() {
  let $ = window.$;
  $(document).ready(function() {
      $('.form-control-range').ionRangeSlider({
          type: "single",
          min: $(this).data('min'),
          max: $(this).data('max'),
          from: 50,
          prefix: $(this).data('prefix'),
          postfix: $(this).data('postfix'),
          keyboard: true
      });
  });
}