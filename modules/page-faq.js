export function page_faq() {
  var $ = window.$;
  $(document).ready(function() {
    // Store filters selector
    var filters = "#filters .btn-filter";

    // Bind collapsers link
    $(filters).on("click", function() {
      // Store the questions selector to display
      var questions_to_display = "." + $(this).data("filter");

      // Remove current active button
      $(filters + ".btn-ecored")
        .removeClass("btn-ecored")
        .addClass("btn-secondary");

      // Set new active button
      $(this)
        .removeClass("btn-secondary")
        .addClass("btn-ecored");

      // Hide all questions that are not affected by the selector
      $(".question")
        .not(questions_to_display)
        .removeClass("d-block")
        .addClass("d-none");

      // Test if the button is the one that displays all the questions
      if (questions_to_display == ".faq_toutes") {
        // First, we cleanup all class already set
        $(".question")
          .removeClass("d-none")
          .removeClass("d-block");

        // Then display all question
        $(".question").addClass("d-block");
      } else {
        // Else, display all question that have been affected by the selector
        $(".question" + questions_to_display)
          .removeClass("d-none")
          .addClass("d-block");
      }
    });

    // Bind collapsers link
    $("#collapsers .btn-link").on("click", function() {
      // Test the data-display attribute of the link that have been clicked
      if ($(this).data("display") == "hide") {
        // If the value of the attribute is 'hide' we
        $(".collapse").removeClass("show");
      } else {
        // Otherwise we consider that everything must be displayed
        $(".collapse").addClass("show");
      }
    });
  });
}
